#!/bin/sh
addgroup -g 101 -S nginx
adduser -S -D -H -u 101 -h /var/cache/nginx -s /sbin/nologin -G nginx -g nginx nginx
mkdir -p /run/nginx
chown nginx:nginx /run/nginx
cp /default.conf /etc/nginx/http.d

exec nginx -g "daemon off;"
