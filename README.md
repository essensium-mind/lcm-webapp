# Example web application in a container

## Build and container repo update

### build script

For the build you'll need **[buildah](https://buildah.io/blogs/2017/11/02/getting-started-with-buildah.html)** installed.

Use:

``./build.sh <a chosen tag suffix>``

Script will build ``amd64`` and ``arm64`` images and upload them to registry: https://gitlab.com/essensium-mind/lcm-webapp/container_registry


Images are uploaded to registry with ``<arch>_<tag_suffix>``

### build by docker

Docker build seems faster and provides simillar results.

To do build you will need ``qemu-user-binfmt`` or ``qemu-user-static`` installed.

#### build process:

``docker buildx build --platform=linux/arm64 -t registry.gitlab.com/essensium-mind/lcm-webapp:<your_tag> . --load``

``docker push registry.gitlab.com/essensium-mind/lcm-webapp:<your_tag>``
