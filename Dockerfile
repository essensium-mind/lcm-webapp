FROM docker.io/library/alpine

RUN apk add --no-cache \
    nginx

COPY images      /usr/share/nginx/html/images
COPY javascript  /usr/share/nginx/html/javascript
COPY json        /usr/share/nginx/html/json
COPY stylesheets /usr/share/nginx/html/stylesheets
COPY overview.html  /usr/share/nginx/html/
COPY default.conf /
COPY startup.sh /
ENTRYPOINT ["/startup.sh"]

