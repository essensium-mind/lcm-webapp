#! /bin/sh

if [ $# -ne 1 ]; then
    echo "Wrong number of arguments"
    echo "Usage: $0 tag_name"
    exit 1
fi

: "${WEBAPP_BUILD_PATH:="."}"
: "${WEBAPP_IMAGE_NAME:="registry.gitlab.com/essensium-mind/lcm-webapp"}"
: "${WEBAPP_IMAGE_TAG:="$1"}"
: "${WEBAPP_ARCHS:="amd64 arm64"}"

buildah --version > /dev/null 2>&1 || {
    printf "Make sure 'buildah' is installed"
    exit 1
}

qemu-aarch64-static --version > /dev/null 2>&1 ||  {
    printf "Make sure 'qemu-user-static' is installed"
    exit 1
}

set -e

for arch in ${WEBAPP_ARCHS}; do
    tag="${WEBAPP_IMAGE_NAME}:${arch}_${WEBAPP_IMAGE_TAG}"
    echo $tag
    buildah build --layers --tag "${tag}" --arch "${arch}" "${WEBAPP_BUILD_PATH}"
    buildah push "${tag}"
done

